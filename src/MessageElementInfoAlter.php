<?php

namespace Drupal\better_messages;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Render\Element\StatusMessages;

/**
 * Replace the default pre_render with MessageElementInfoAlter.
 */
class MessageElementInfoAlter implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'preRender',
      'renderMessages',
    ];
  }

  /**
   * Render placeholder.
   *
   * @param array $element
   *   Render array.
   *
   * @return array
   *   Render array with the placeholder.
   *
   * @see better_messages_element_info_alter()
   */
  public static function preRender(array $element) {
    // The code below is heavily copied from StatusMessages::renderMessages().
    $element = [
      '#lazy_builder' => [
        get_class() . '::renderMessages',
        [$element['#display']],
      ],
      '#create_placeholder' => TRUE,
    ];

    // Directly create a placeholder as we need this to be placeholdered
    // regardless if this is a POST or GET request.
    // @todo remove this when https://www.drupal.org/node/2367555 lands.
    return \Drupal::service('render_placeholder_generator')->createPlaceholder($element);
  }

  /**
   * Lazy builder for 'status_messages'.
   *
   * Improved version of StatusMessages::renderMessages()
   * that additionally wraps the output into an overlay
   * markup whenever necessary.
   *
   * @param string|null $type
   *   Limit the messages returned by type. Defaults to NULL, meaning all types.
   *   Passed on to drupal_get_messages(). These values are supported:
   *   - NULL
   *   - 'status'
   *   - 'warning'
   *   - 'error'.
   *
   * @return array
   *   A renderable array containing the messages.
   */
  public static function renderMessages($type) {
    $render = StatusMessages::renderMessages($type);
    if (!empty($render)) {
      $render['#theme_wrappers'][] = 'better_messages_wrapper';
      $render['#attached']['library'][] = 'better_messages/better_messages';
      $render['#attached']['drupalSettings']['better_messages'] = \Drupal::config('better_messages.settings')->get();
    }

    return $render;
  }

}

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Better Messages is a very simple module that provides "Popup-like" Drupal messages.
Better Messages intends to let you control where and how you want to see your Drupal messages! Combine different animations and use own better-messages-wrapper.html.twig to create your custom look.

FEATURES
--------
- Control the position and width of the popup message relative to screen.
- Control the animation for opening, and for closing the popup message.
- Enable or disable popup messages at specific pages.
- Works with AHAH submits. COOL!!
- Override the popup message by coping and altering better_messages.tpl.php into your theme directory.
- Override the popup message CSS by coping and altering better_messages.css file in your theme directory.
- Optional dependency on jQuery UI module which can make the message draggable.
- Control countdown timer for auto closing messages.
- Cross browser support! Works with any browser you can possible imagine.
 -This modules degrades gracefully for browsers with JavaScript disabled.

RECOMMENDED MODULES
-------------------

 * No extra module is required.


INSTALLATION
------------

Install as usual, see [Installing Drupal Module](https://www.drupal.org/node/1897420)


CONFIGURATION
-------------

Customize the settings of popup messages in Administration » Configuration »
User interface » Better Message settings. (/admin/config/user-interface/better-messages)
- Messages positions and basic properties
  - Set position of Message -Center screen
  - Center screen
  - Keep fixed position of message as you scroll
  - Custom width
  - Left/Right spacing
  - Top/Down spacing
  - more...  
- Messages animation settings
  - Pop-in (show) message box effect
  - Duration of (show) effect
  - Number of seconds to auto close after the page has loaded  
  - Disable auto close if messages include an error message  
  - Show countdown timer  
  - Stop auto close timer when hover  
  - Number of seconds to delay message after the page has loaded
  - more... 
- jQuery UI enhancements
  
The Drupal standard messages will be replaced automatically.
